import { TestBed } from '@angular/core/testing';

import { RegisterentrepriseService } from './registerentreprise.service';

describe('RegisterentrepriseService', () => {
  let service: RegisterentrepriseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegisterentrepriseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
