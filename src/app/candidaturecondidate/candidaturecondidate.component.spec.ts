import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidaturecondidateComponent } from './candidaturecondidate.component';

describe('CandidaturecondidateComponent', () => {
  let component: CandidaturecondidateComponent;
  let fixture: ComponentFixture<CandidaturecondidateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CandidaturecondidateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CandidaturecondidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
