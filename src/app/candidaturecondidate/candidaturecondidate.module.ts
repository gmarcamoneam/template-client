import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CandidaturecondidateRoutingModule } from './candidaturecondidate-routing.module';
import { CandidaturecondidateComponent } from './candidaturecondidate.component';


@NgModule({
  declarations: [
    CandidaturecondidateComponent
  ],
  imports: [
    CommonModule,
    CandidaturecondidateRoutingModule
  ]
})
export class CandidaturecondidateModule { }
