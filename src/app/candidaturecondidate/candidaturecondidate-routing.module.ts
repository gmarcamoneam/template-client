import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidaturecondidateComponent } from './candidaturecondidate.component';

const routes: Routes = [{ path: '', component: CandidaturecondidateComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidaturecondidateRoutingModule { }
