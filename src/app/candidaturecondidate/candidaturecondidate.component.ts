import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-candidaturecondidate',
  templateUrl: './candidaturecondidate.component.html',
  styleUrls: ['./candidaturecondidate.component.css'],
})
export class CandidaturecondidateComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  id = this.activeroute.snapshot.params['id'];

  listcondidature: any;
  constructor(
    private offreservice: OffreService,
    private activeroute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getallcondidatures();
  }
  getallcondidatures() {
    this.offreservice.getallcondidature().subscribe((res: any) => {
      this.listcondidature = res['data'].filter(
        (el: any) => el.condidate._id == this.userconnect._id
      );
      console.log('my list of condidature', this.listcondidature);
    });
  }
  deletecondidatures(id: any) {
    this.offreservice.deletecandidature(id).subscribe((res: any) => {
      console.log(res);
      Swal.fire('Deleted!');
    });
  }
}
