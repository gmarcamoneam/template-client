import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
form :FormGroup
token=this.activateroute.snapshot.params["token"]
  constructor(private activateroute:ActivatedRoute,private formbuilder :FormBuilder,private resetservice:RegisterentrepriseService,
    private route:Router) { }

  ngOnInit(): void {
    this.form=this.formbuilder.group({
      newPass:['',Validators.required]
    })
  }
  resetpassword(){
    this.resetservice.resetPassword(this.token,this.form.value).subscribe((res:any)=>{
      console.log(res)
     // this.token=res["data"]
      Swal.fire("your password was changed ")
     
      this.route.navigateByUrl('/login')
    })
  }

}
