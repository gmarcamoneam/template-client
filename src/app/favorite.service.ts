import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
items=[] as any
  constructor() { }
  addtofavirit(addoffre:any){
    this.items.push(addoffre)
    this.savefavorit()
  }
  offreinfavorit(offre:any):boolean{
    return this.items.findIndex((o:any)=>o._id ==offre._id)>-1
  }
  loadfavorit():void{
    this.items=JSON.parse(localStorage.getItem('favorit_items')!) ?? []
  }
  getitems(){
    return this.items
  }
  savefavorit():void{
    localStorage.setItem('favorit_items',JSON.stringify(this.items))
  }
  removefavorit(offre:any){
    console.log(this.items)
    const index = this.items.findIndex((o:any)=>o._id===offre._id)
    if(index>-1){
      this.items.splice(index,1)
      this.savefavorit()
    }
  }
}
