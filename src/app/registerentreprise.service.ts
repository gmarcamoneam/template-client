import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterentrepriseService {

  constructor(private http:HttpClient) { }
 /*   token = localStorage.getItem('token')!
  headersoption = new HttpHeaders({
    Authorization:'Bearer ' + this.token
  }) */ 
  registerentreprise(entreprises:any){
    return this.http.post(`${environment.baseurl}/registreentreprise`,entreprises)
  }
  registercondidate(condidate:any){
    return this.http.post(`${environment.baseurl}/registre`,condidate)
  }
  loginentreprise(login:any){
    return this.http.post(`${environment.baseurl}/loginentreprise`,login)
  }
  login(login:any){
    return this.http.post(`${environment.baseurl}/login`,login)
  }
  getallentreprise(){
    return this.http.get(`${environment.baseurl}/getAllentreprises`)
    
  }
  forgetpossword(email:any){
    return this.http.post(`${environment.baseurl}/forgetpassword`,email)
  }
  resetPassword(resetPasswordToken:any,newpassword:any){
    return this.http.post(`${environment.baseurl}/resetpassword/${resetPasswordToken}`,newpassword)
  }
   updateprofil(id:any,profil:any){
    return this.http.put(`${environment.baseurl}/updateuser/${id}`,profil/*,{headers:this.headersoption}*/)
  } 
  getUserById(id:any){
  return this.http.get(`${environment.baseurl}/user/${id}`)
  }
  deletecondidate(id:any){    
    return this.http.delete(`${environment.baseurl}/deletecondidate/${id}`/*,{headers:this.headersoption}*/)
  }
  deleteentreprise(id:any){    
    return this.http.delete(`${environment.baseurl}/deleteentreprise/${id}`/*,{headers:this.headersoption}*/)
  }
}
