import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileentrepriseRoutingModule } from './profileentreprise-routing.module';
import { ProfileentrepriseComponent } from './profileentreprise.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProfileentrepriseComponent
  ],
  imports: [
    CommonModule,
    ProfileentrepriseRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProfileentrepriseModule { }
