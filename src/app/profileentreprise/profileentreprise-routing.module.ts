import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileentrepriseComponent } from './profileentreprise.component';

const routes: Routes = [{ path: '', component: ProfileentrepriseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileentrepriseRoutingModule { }
