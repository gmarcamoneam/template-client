import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-profileentreprise',
  templateUrl: './profileentreprise.component.html',
  styleUrls: ['./profileentreprise.component.css']
})
export class ProfileentrepriseComponent implements OnInit {
  userconnect=JSON.parse(localStorage.getItem("userconnect")!)

  formupdate:FormGroup
  p:any
  filetoupload:Array<File>=[] 
  constructor(private formbilder:FormBuilder,private registerservice:RegisterentrepriseService,
    private route:Router) { }

  ngOnInit(): void {
      //update
      this.formupdate=this.formbilder.group({
           fullname:['',Validators.required],
           email:['',Validators.required],
            photo:['',Validators.required],
           phone:['',Validators.required], 
           website:['',Validators.required], 
           
          
         }) 
         this.formupdate.patchValue({
          
           fullname:this.userconnect.fullname,
           email:this.userconnect.email,
           photo:this.userconnect.photo,         
           phone:this.userconnect.phone,
           website:this.userconnect.website,
         })
       }


handlefileinput(files:any){
  this.filetoupload=<Array<File>>files.target.files
  console.log(this.filetoupload)
} 


updateentreprise(){ //formupdate:FormGroup
this.registerservice.updateprofil(this.userconnect._id,this.formupdate.value).subscribe((res:any)=>{
console.log(res)
localStorage.setItem('userconnect',JSON.stringify(res.data)) //bech ta3mel refrech l localstage
Swal.fire("profile updated")
})
}
deleteentreprise(id:any){
  this.registerservice.deleteentreprise(id).subscribe((res:any)=>{
    localStorage.clear()
    console.log(res)
  Swal.fire( 'are you sure delete!','yes')
  this.route.navigateByUrl('/login')
})
}
}