import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileentrepriseComponent } from './profileentreprise.component';

describe('ProfileentrepriseComponent', () => {
  let component: ProfileentrepriseComponent;
  let fixture: ComponentFixture<ProfileentrepriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileentrepriseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProfileentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
