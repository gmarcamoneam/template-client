import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


//import { RecherchePipe } from './pipes/recherche.pipe';


@NgModule({
  declarations: [
    AppComponent,
    //RecherchePipe,
   // Ng2SearchPipeModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
   
   // RecherchePipe,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
