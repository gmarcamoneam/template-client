import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListcondidatureComponent } from './listcondidature.component';

describe('ListcondidatureComponent', () => {
  let component: ListcondidatureComponent;
  let fixture: ComponentFixture<ListcondidatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListcondidatureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListcondidatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
