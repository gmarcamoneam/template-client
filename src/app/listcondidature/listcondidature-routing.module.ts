import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListcondidatureComponent } from './listcondidature.component';

const routes: Routes = [{ path: '', component: ListcondidatureComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListcondidatureRoutingModule { }
