import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListcondidatureRoutingModule } from './listcondidature-routing.module';
import { ListcondidatureComponent } from './listcondidature.component';


@NgModule({
  declarations: [
    ListcondidatureComponent
  ],
  imports: [
    CommonModule,
    ListcondidatureRoutingModule
  ]
})
export class ListcondidatureModule { }
