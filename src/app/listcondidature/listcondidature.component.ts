import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-listcondidature',
  templateUrl: './listcondidature.component.html',
  styleUrls: ['./listcondidature.component.css'],
})
export class ListcondidatureComponent implements OnInit {
  id = this.active.snapshot.params['id'];
  offre: any;

  accepter: any;
  date = new Date().toISOString().split('T')[0].toString();
  constructor(private offres: OffreService, private active: ActivatedRoute) {}

  ngOnInit(): void {
    this.getoffresbyid();
  }

  getoffresbyid() {
    this.offres.getoffrebyid(this.id).subscribe((res: any) => {
      this.offre = res['data'];
      console.log('offre by id', this.offre);
    });
  }

  deletecondidatures(id: any) {
    this.offres.deletecandidature(id).subscribe((res: any) => {
      console.log(res);
      Swal.fire('Deleted!');
    });
  }
  acceptcondidatures(id: any) {
    this.offres.acceptcondidature(id).subscribe(() => {
      this.accepter;
    });
  }
}
