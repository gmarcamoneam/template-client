import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OffreService } from '../offre.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { animate } from '@angular/animations';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addoffre',
  templateUrl: './addoffre.component.html',
  styleUrls: ['./addoffre.component.css'],
})
export class AddoffreComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  offre: FormGroup;
  listcontrat: any;
  listcategory: any;
  listplace: any;
  listskills: any;
  listentreprise: any;
  p: any;
  constructor(
    private formbuilder: FormBuilder,
    private offreser: OffreService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.getallcategorys();
    this.getallcontrats();
    this.getallskill();
    this.getallplace();
    this.getallentreprises();
    this.offre = this.formbuilder.group({
      titre: ['', Validators.required],
      description: ['', Validators.required],
      salaire: ['', Validators.required],
      dateexpiration: ['', Validators.required],
      category: ['', Validators.required],
      skill: ['', Validators.required],
      contrat: ['', Validators.required],
      place: ['', Validators.required],
      entreprise: ['', Validators.required],
    });
  }
  createoffre() {
    this.offre.patchValue({
      entreprise: this.userconnect._id,
    });
    this.offreser.addoffre(this.offre.value).subscribe((res: any) => {
      console.log(res['data']);
      console.log('offre added');
      Swal.fire('creation avec succes attend la validation');
    });
  }
  getallplace() {
    this.offreser.getallplace().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('list of place', this.listplace);
    });
  }
  getallcategorys() {
    this.offreser.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
      console.log('list of category', this.listcategory);
    });
  }
  getallskill() {
    this.offreser.getallskills().subscribe((res: any) => {
      this.listskills = res['data'];
      console.log('list of skills', this.listskills);
    });
  }
  getallcontrats() {
    this.offreser.getallcontrat().subscribe((res: any) => {
      this.listcontrat = res['data'];
      console.log('list of contrat', this.listcontrat);
    });
  }
  getallentreprises() {
    this.offreser.getallentreprise().subscribe((res: any) => {
      this.listentreprise = res['data'].filter(
        (el: any) => el.confirmed == true
      );
      console.log('list of entreprise', this.listentreprise);
    });
  }
}
