import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddoffreComponent } from './addoffre.component';

const routes: Routes = [{ path: '', component: AddoffreComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddoffreRoutingModule { }
