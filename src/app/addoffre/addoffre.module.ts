import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddoffreRoutingModule } from './addoffre-routing.module';
import { AddoffreComponent } from './addoffre.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AddoffreComponent
  ],
  imports: [
    CommonModule,
    AddoffreRoutingModule,
    FormsModule,
    ReactiveFormsModule //[formGroup]="offre"
  ]
})
export class AddoffreModule { }
