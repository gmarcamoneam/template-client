import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CondidateComponent } from './condidate.component';

const routes: Routes = [{ path: '', component: CondidateComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CondidateRoutingModule { }
