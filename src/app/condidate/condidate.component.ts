import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-condidate',
  templateUrl: './condidate.component.html',
  styleUrls: ['./condidate.component.css'],
})
export class CondidateComponent implements OnInit {
  condidate: FormGroup;

  filetoupload: Array<File> = [];
  constructor(
    private formbuilder: FormBuilder,
    private registerservice: RegisterentrepriseService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.condidate = this.formbuilder.group({
      fullname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      photo: ['', Validators.required],
      phone: ['', Validators.required],
      git: ['', Validators.required],
      linkedIn: ['', Validators.required],
      adresse: ['', Validators.required],
    });
  }
  handlefileinput(files: any) {
    this.filetoupload = <Array<File>>files.target.files;
    console.log(this.filetoupload);
  }

  registercondidate() {
    let formdata = new FormData();
    formdata.append('fullname', this.condidate.value.fullname);
    formdata.append('email', this.condidate.value.email);
    formdata.append('password', this.condidate.value.password);

    formdata.append('git', this.condidate.value.git);
    formdata.append('phone', this.condidate.value.phone);
    formdata.append('linkedIn', this.condidate.value.linkedIn);
    formdata.append('adresse', this.condidate.value.adresse);
    formdata.append('photo', this.filetoupload[0]);
    this.registerservice.registercondidate(formdata).subscribe((res: any) => {
      console.log(res['data']);
      console.log('condidate created');
      this.route.navigateByUrl('/login');
    });
  }
}
