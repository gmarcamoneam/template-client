import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CondidateRoutingModule } from './condidate-routing.module';
import { CondidateComponent } from './condidate.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CondidateComponent
  ],
  imports: [
    CommonModule,
    CondidateRoutingModule,
    ReactiveFormsModule,
  ]
})
export class CondidateModule { }
