import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpdateoffreRoutingModule } from './updateoffre-routing.module';
import { UpdateoffreComponent } from './updateoffre.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UpdateoffreComponent
  ],
  imports: [
    CommonModule,
    UpdateoffreRoutingModule,
    ReactiveFormsModule
  ]
})
export class UpdateoffreModule { }
