import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpdateoffreComponent } from './updateoffre.component';

const routes: Routes = [{ path: '', component: UpdateoffreComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateoffreRoutingModule { }
