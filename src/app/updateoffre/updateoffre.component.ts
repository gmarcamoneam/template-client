import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-updateoffre',
  templateUrl: './updateoffre.component.html',
  styleUrls: ['./updateoffre.component.css']
})
export class UpdateoffreComponent implements OnInit {
  userconnect=JSON.parse(localStorage.getItem("userconnect")!)

  id=this.active.snapshot.params["id"]
  form:FormGroup
  formm:FormGroup
  p:any
  constructor(private offreservise:OffreService, private active:ActivatedRoute,private formbuilder:FormBuilder) { }

  ngOnInit(): void {
    //this. getoffrebyid()
    this.form=this.formbuilder.group(
      {
        _id:['',Validators.required],
        titre:['',Validators.required],
        description:['',Validators.required],
        dateexpiration:['',Validators.required],
        salaire:['',Validators.required],
        contrat:['',Validators.required],
        skill:['',Validators.required],
        category:['',Validators.required],
        place:['',Validators.required],
      }
     
    )

  }

  open(offre:any){
    this.form.patchValue({
      id:offre.id,
     titre:offre.titre,
     description:offre.description,
     dateexpiration:offre.dateexpiration,
     salaire:offre.salaire,
    contrat:offre.contrat,
    skill:offre.skill,
    category:offre.category,
    place:offre.place,
    
   }) 
  }
  

  updateoffres(){
    this.offreservise.updateoffre(this.id,this.formm.value).subscribe((res:any)=>{
      console.log(res)
     // localStorage.setItem('userconnect',JSON.stringify(res.data))
      Swal.fire("offre updated")
    
    })
  }

}
