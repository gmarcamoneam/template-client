import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  constructor(
    private registerentrprise: RegisterentrepriseService,
    private formbuilder: FormBuilder,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.login = this.formbuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  logins() {
    this.registerentrprise.login(this.login.value).subscribe((res: any) => {
      console.log(res);
      Swal.fire('connected');
      if (res.success === true) {
        localStorage.setItem('userconnect', JSON.stringify(res.user));
        localStorage.setItem('token', res.token);
        localStorage.setItem('refrechtoken', res.refreshToken);
        localStorage.setItem('state', '0');
        localStorage.setItem('role', res.user.role);
        console.log(res);
        if (res.user.role == 'condidate') {
          this.route.navigateByUrl('/');
        } else {
          this.route.navigateByUrl('/offreentreprise');
        }
      }
    });
  }
}
