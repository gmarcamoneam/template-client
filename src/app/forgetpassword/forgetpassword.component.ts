import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css'],
})
export class ForgetpasswordComponent implements OnInit {
  forget: any;
  form: FormGroup;
  token: any;
  constructor(
    private register: RegisterentrepriseService,
    private formbuilder: FormBuilder,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.form = this.formbuilder.group({
      email: ['', Validators.required],
    });
  }
  forgetpasswordd() {
    console.log(this.form.value);
    this.register.forgetpossword(this.form.value).subscribe((res: any) => {
      console.log(res);
      Swal.fire('check your email');
      this.route.navigateByUrl('/resetpassword/' + res.token);
    });
  }
}
