import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListoffreRoutingModule } from './listoffre-routing.module';
import { ListoffreComponent } from './listoffre.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    ListoffreComponent
  ],
  imports: [
    CommonModule,
    ListoffreRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule
   // Ng2SearchPipeModule,
   // NgbModule
  ]
})
export class ListoffreModule { }
