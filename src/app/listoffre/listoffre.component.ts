import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FavoriteService } from '../favorite.service';
@Component({
  selector: 'app-listoffre',
  templateUrl: './listoffre.component.html',
  styleUrls: ['./listoffre.component.css'],
})
export class ListoffreComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  listoffre: any;
  listcontrat: any;
  listplace: any;
  p: any;
  term: any;
  modale: FormGroup;
  items = [] as any;
  filetoupload: Array<File> = [];
  date = new Date().toISOString().split('T')[0].toString();
  constructor(
    private offreservice: OffreService,
    private formbuilder: FormBuilder,
    private route: Router,
    private favoritservice: FavoriteService
  ) {}

  ngOnInit(): void {
    this.getallcontrats();
    this.getalloffres();
    this.getallplaces();
    this.modale = this.formbuilder.group({
      photo: ['', Validators.required],
      offre: ['', Validators.required],
      condidate: ['', Validators.required],
    });
  }
  getalloffres() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter((element: any) => {
        return (
          element.confirm == true &&
          Date.parse(element.dateexpiration) >= Date.parse(this.date)
        );
      });
      console.log('list of offre', this.listoffre);
    });
  }
  getallplaces() {
    this.offreservice.getallplace().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('list of place', this.listplace);
    });
  }
  getallcontrats() {
    this.offreservice.getallcontrat().subscribe((res: any) => {
      this.listcontrat = res['data'];
      console.log('list of contrat', this.listcontrat);
    });
  }
  handlefileinput(files: any) {
    this.filetoupload = <Array<File>>files.target.files;
    console.log(this.filetoupload);
  }
  createcondidature() {
    let formdata = new FormData();
    formdata.append('condidate', this.modale.value.condidate);
    formdata.append('offre', this.modale.value.offre);
    formdata.append('photo', this.filetoupload[0]);
    this.offreservice.addcondidature(formdata).subscribe((res: any) => {
      console.log(res['data']);
      Swal.fire('condidature created');
    });
  }
  isconect(p: any) {
    if (!(localStorage.getItem('state') == '0')) {
      this.route.navigateByUrl('/login');
    } else {
      this.open(p);
    }
  }
  open(p: any) {
    this.modale.patchValue({
      photo: p.photo,
      offre: p._id,
      condidate: this.userconnect._id,
    });
  }

  onchangebycontrat(e: any) {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter((el: any) => el.confirm == true);
      this.listoffre = this.listoffre.filter(
        (el: any) => el.contrat._id == e.target.value
      );
      console.log('list of contrat', this.listoffre);
    });
  }
  onchangebyplace(a: any) {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter((el: any) => el.confirm == true);
      this.listoffre = this.listoffre.filter(
        (el: any) => el.place._id == a.target.value
      );
      console.log('list of contrat', this.listoffre);
    });
  }
  addfavorite(offre: any) {
    if (!this.favoritservice.offreinfavorit(offre))
      this.favoritservice.addtofavirit(offre);
    this.items = this.favoritservice.getitems();
    console.log(this.items);
    Swal.fire('offre edded to favorit');
  }
}
