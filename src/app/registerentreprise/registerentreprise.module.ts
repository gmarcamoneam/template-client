import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterentrepriseRoutingModule } from './registerentreprise-routing.module';
import { RegisterentrepriseComponent } from './registerentreprise.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RegisterentrepriseComponent
  ],
  imports: [
    CommonModule,
    RegisterentrepriseRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class RegisterentrepriseModule { }
