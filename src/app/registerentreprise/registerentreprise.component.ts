import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-registerentreprise',
  templateUrl: './registerentreprise.component.html',
  styleUrls: ['./registerentreprise.component.css']
})
export class RegisterentrepriseComponent implements OnInit {
  entreprise:FormGroup
filetoupload:Array<File>=[] 
  constructor(private formbuilder:FormBuilder,
    private register:RegisterentrepriseService,
    private route:Router) { }

  ngOnInit(): void {
    this.entreprise=this.formbuilder.group({
      fullname:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
      photo:['',Validators.required],
      website:['',Validators.required],
      phone:['',Validators.required],
    })
  }
  handlefileinput(files:any){
    this.filetoupload=<Array<File>>files.target.files
    console.log(this.filetoupload)
  }
   createentrceprise(){
    let formdata=new FormData()
    formdata.append("fullname",this.entreprise.value.fullname)
    formdata.append("email",this.entreprise.value.email)
    formdata.append("password",this.entreprise.value.password)
    formdata.append("website",this.entreprise.value.website)
    formdata.append("phone",this.entreprise.value.phone)
    formdata.append("photo",this.filetoupload[0])
    this.register.registerentreprise(formdata).subscribe((res:any)=>{
      console.log(res['data'])
      console.log('entreprise created')
       this.route.navigateByUrl('/login') 
    })
  } 
}
  

