import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterentrepriseComponent } from './registerentreprise.component';

const routes: Routes = [{ path: '', component: RegisterentrepriseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterentrepriseRoutingModule { }
