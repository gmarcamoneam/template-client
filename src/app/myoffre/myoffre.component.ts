import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-myoffre',
  templateUrl: './myoffre.component.html',
  styleUrls: ['./myoffre.component.css'],
})
export class MyoffreComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  listoffre: any;
  entreprise: any;
  modale: FormGroup;
  filetoupload: Array<File> = [];
  id = this.activatroute.snapshot.params['id'];
  constructor(
    private activatroute: ActivatedRoute,
    private register: RegisterentrepriseService,
    private formbuilder: FormBuilder,
    private offreservice: OffreService
  ) {}

  ngOnInit(): void {
    this.getUserById();
    this.modale = this.formbuilder.group({
      photo: ['', Validators.required],
      offre: ['', Validators.required],
      condidate: ['', Validators.required],
    });
  }
  getUserById() {
    this.register.getUserById(this.id).subscribe((res: any) => {
      this.entreprise = res['data'];
      console.log('entreprise:', this.entreprise);
    });
  }
  open(p: any) {
    this.modale.patchValue({
      photo: p.photo,
      offre: p._id,
      condidate: this.userconnect._id,
    });
  }
  handlefileinput(files: any) {
    this.filetoupload = <Array<File>>files.target.files;
    console.log(this.filetoupload);
  }
  createcondidature() {
    let formdata = new FormData();
    formdata.append('condidate', this.modale.value.condidate);
    formdata.append('offre', this.modale.value.offre);
    formdata.append('photo', this.filetoupload[0]);
    this.offreservice.addcondidature(formdata).subscribe((res: any) => {
      console.log(res['data']);
      Swal.fire('condidature created');
    });
  }
}
