import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyoffreComponent } from './myoffre.component';

describe('MyoffreComponent', () => {
  let component: MyoffreComponent;
  let fixture: ComponentFixture<MyoffreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyoffreComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyoffreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
