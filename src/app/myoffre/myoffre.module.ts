import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyoffreRoutingModule } from './myoffre-routing.module';
import { MyoffreComponent } from './myoffre.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MyoffreComponent
  ],
  imports: [
    CommonModule,
    MyoffreRoutingModule,
    ReactiveFormsModule
  ]
})
export class MyoffreModule { }
