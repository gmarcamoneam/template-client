import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyoffreComponent } from './myoffre.component';

const routes: Routes = [{ path: '', component: MyoffreComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyoffreRoutingModule { }
