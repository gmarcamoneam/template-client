import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-offreentreprise',
  templateUrl: './offreentreprise.component.html',
  styleUrls: ['./offreentreprise.component.css'],
})
export class OffreentrepriseComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  id = this.active.snapshot.params['id'];
  date = new Date().toISOString().split('T')[0].toString();

  formm: FormGroup;
  form: FormGroup;
  entreprise: any;
  listoffre: any;
  listcondidate: any;
  listcondidature: any;
  listplace: any;

  listcontrat: any;
  offres: any;
  listcategory: any;
  listskill: any;
  constructor(
    private offre: OffreService,
    private formbuilder: FormBuilder,
    private register: RegisterentrepriseService,
    private active: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getUserById();
    this.getallcontrats();
    this.getallskill();
    this.getallcategorys();
    this.getallplaces();
    this.form = this.formbuilder.group({
      _id: ['', Validators.required],
      titre: ['', Validators.required],
      description: ['', Validators.required],
      dateexpiration: ['', Validators.required],
      salaire: ['', Validators.required],
      contrat: ['', Validators.required],
      skill: ['', Validators.required],
      category: ['', Validators.required],
      place: ['', Validators.required],
    });
  }

  getUserById() {
    this.register.getUserById(this.userconnect._id).subscribe((res: any) => {
      this.entreprise = res['data'];
      console.log('entreprise:', this.entreprise);
    });
  }
  deleteoffres(id: any) {
    this.offre.deleteoffre(id).subscribe((res: any) => {
      console.log(res);
      Swal.fire('Deleted!');
    });
  }

  updateoffres() {
    this.offre
      .updateoffre(this.form.value._id, this.form.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('offre updated');
      });
  }
  open(offres: any) {
    this.form.patchValue({
      _id: offres._id,
      titre: offres.titre,
      description: offres.description,
      dateexpiration: offres.dateexpiration,
      salaire: offres.salaire,
      contrat: offres.contrat._id,
      skill: offres.skill._id,
      category: offres.category._id,
      place: offres.place._id,
    });
  }
  getallcondidature() {
    this.offre.getallcondidature().subscribe((res: any) => {
      this.listcondidature = res['data'];
      console.log('list of condidature', this.listcondidature);
    });
  }
  getallplaces() {
    this.offre.getallplace().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('list of place', this.listplace);
    });
  }
  getallcontrats() {
    this.offre.getallcontrat().subscribe((res: any) => {
      this.listcontrat = res['data'];
      console.log('list of contrat', this.listcontrat);
    });
  }
  getallcategorys() {
    this.offre.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
      console.log('list of category', this.listcategory);
    });
  }
  getallskill() {
    this.offre.getallskills().subscribe((res: any) => {
      this.listskill = res['data'];
      console.log('list of category', this.listskill);
    });
  }
}
