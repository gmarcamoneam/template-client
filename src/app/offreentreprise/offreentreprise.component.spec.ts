import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffreentrepriseComponent } from './offreentreprise.component';

describe('OffreentrepriseComponent', () => {
  let component: OffreentrepriseComponent;
  let fixture: ComponentFixture<OffreentrepriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OffreentrepriseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OffreentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
