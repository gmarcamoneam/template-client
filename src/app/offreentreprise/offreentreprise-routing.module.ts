import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OffreentrepriseComponent } from './offreentreprise.component';

const routes: Routes = [{ path: '', component: OffreentrepriseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffreentrepriseRoutingModule { }
