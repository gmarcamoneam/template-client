import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffreentrepriseRoutingModule } from './offreentreprise-routing.module';
import { OffreentrepriseComponent } from './offreentreprise.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    OffreentrepriseComponent
  ],
  imports: [
    CommonModule,
    OffreentrepriseRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    
  ]
})
export class OffreentrepriseModule { }
