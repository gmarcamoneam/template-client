import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FavoriteService } from '../favorite.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
})
export class FavoriteComponent implements OnInit {
  items = [] as any;
  p: any;
  constructor(private favoritservice: FavoriteService, private route: Router) {}

  ngOnInit(): void {
    this.favoritservice.loadfavorit();
    this.items = this.favoritservice.getitems();
    console.log('items', this.items);
  }
  removefavorit(favorite: any) {
    this.favoritservice.removefavorit(favorite);
    Swal.fire('deleted !!');
  }
}
