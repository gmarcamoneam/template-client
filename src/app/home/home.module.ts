import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { LayoutComponent } from '../component/layout/layout.component';
import { FooterComponent } from '../component/footer/footer.component';
import { HeaderComponent } from '../component/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RecherchePipe } from '../pipes/recherche.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    HomeComponent,
    LayoutComponent,
    FooterComponent,
    HeaderComponent,
    RecherchePipe
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
   // NgbModule
   Ng2SearchPipeModule,
   ReactiveFormsModule,
   FormsModule
  ]
})
export class HomeModule { }
