import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddoffreComponent } from '../addoffre/addoffre.component';
import { CandidaturecondidateComponent } from '../candidaturecondidate/candidaturecondidate.component';
import { LayoutComponent } from '../component/layout/layout.component';
import { CondidateComponent } from '../condidate/condidate.component';
import { DetailoffreComponent } from '../detailoffre/detailoffre.component';
import { FavoriteComponent } from '../favorite/favorite.component';
import { ForgetpasswordComponent } from '../forgetpassword/forgetpassword.component';
import { GoogleComponent } from '../google/google.component';
import { AuthGuard } from '../guards/auth.guard';
import { ListcondidatureComponent } from '../listcondidature/listcondidature.component';
import { ListentrepriseComponent } from '../listentreprise/listentreprise.component';
import { ListoffreComponent } from '../listoffre/listoffre.component';
import { LoginComponent } from '../login/login.component';
import { MyoffreComponent } from '../myoffre/myoffre.component';
import { OffreentrepriseComponent } from '../offreentreprise/offreentreprise.component';
import { ProfilecondidateComponent } from '../profilecondidate/profilecondidate.component';
import { ProfileentrepriseComponent } from '../profileentreprise/profileentreprise.component';
import { RegisterentrepriseComponent } from '../registerentreprise/registerentreprise.component';
import { ResetpasswordComponent } from '../resetpassword/resetpassword.component';
import { UpdateoffreComponent } from '../updateoffre/updateoffre.component';
import { HomeComponent } from './home.component';

const routes: Routes = [ 
  
  { path: '', component: HomeComponent,children:[
  {path:'',component:LayoutComponent},
  {path:'listoffre',component:ListoffreComponent},
  {path:'addoffre',component:AddoffreComponent},
  {path:'registerentreprise',component:RegisterentrepriseComponent},
  {path:'login',component:LoginComponent},
  {path:'condidate',component:CondidateComponent},
  {path:'detailoffre/:id',component:DetailoffreComponent},
  {path:'profilecondidate',canActivate:[AuthGuard],component:ProfilecondidateComponent},
  {path:'profileentreprise',component:ProfileentrepriseComponent},
  {path:'offreentreprise',component:OffreentrepriseComponent},
  {path:'forgetpassword',component:ForgetpasswordComponent},
  {path:'resetpassword/:token',component:ResetpasswordComponent},
  {path:'favorite',component:FavoriteComponent},
  {path:'updateoffre/:id',component:UpdateoffreComponent},
  {path:'listcondidature/:id',component:ListcondidatureComponent},
  {path:'candidaturecondidate',component:CandidaturecondidateComponent},
  {path:'listentreprise',component:ListentrepriseComponent},
  {path:'myoffre/:id',component:MyoffreComponent},
  {path:'google',component:GoogleComponent}



  





  



] 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
