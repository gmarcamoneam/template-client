import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OffreService {

  constructor(private http:HttpClient) { }
/*    token = localStorage.getItem('token')!
  headersoption = new HttpHeaders({
    Authorization:'Bearer ' + this.token
  })  */
  getalloffre(){
    return this.http.get(`${environment.baseurl}/getAlloffres`)
  }
  addoffre(offre:any){
    return this.http.post(`${environment.baseurl}/createoffre`,offre)
  }
  getoffrebyid(id:any){
    return this.http.get(`${environment.baseurl}/getoffreById/${id}`)
  }
  getallplace(){
    return this.http.get(`${environment.baseurl}/getAllplaces`)
  }
  getallcategory(){
    return this.http.get(`${environment.baseurl}/getAllCategories`)
  }
  getallcontrat(){
    return this.http.get(`${environment.baseurl}/getAllContrats`)
  }
  getallskills(){
    return this.http.get(`${environment.baseurl}/getAllSkills`)
  }
  getallentreprise(){
    return this.http.get(`${environment.baseurl}/getAllentreprises`)

  }
  addcondidature(condidature:any){
    return this.http.post(`${environment.baseurl}/createcandidature`,condidature)
  }
   searchoffre(titre:any){
    return this.http.get(`${environment.baseurl}/getoffreByTitre`,titre)
  } 
   
    getallentrepris(){
      return this.http.get(`${environment.baseurl}/getAllentreprises`)
    }
    getallcondidate(){
      return this.http.get(`${environment.baseurl}/getAllcondidate`)
    }
    deleteoffre(id:any){    
      return this.http.delete(`${environment.baseurl}/deleteoffres/${id}`/*,{headers:this.headersoption}*/)
    }
    updateoffre(id:any,offre:any){
      return this.http.put(`${environment.baseurl}/updateoffres/${id}`,offre/*,{headers:this.headersoption}*/)
    }
    addedcommentaire(commentaire:any){
      return this.http.post(`${environment.baseurl}/createcommentaire`,commentaire)
    }
 
    getallcondidature(){
      return this.http.get(`${environment.baseurl}/getAllcandidatures`)
    }
    deletecandidature(id:any){    
      return this.http.delete(`${environment.baseurl}/deletecandidature/${id}`/*,{headers:this.headersoption}*/)
    }
    acceptcondidature(id:any){
      return this.http.get(`${environment.baseurl}/acceptcondidature/${id}`)
    }
}
