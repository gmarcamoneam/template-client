import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-profilecondidate',
  templateUrl: './profilecondidate.component.html',
  styleUrls: ['./profilecondidate.component.css'],
})
export class ProfilecondidateComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  id = this.activateroute.snapshot.params['id'];

  formupdate: FormGroup;
  p: any;
  filetoupload: Array<File> = [];
  constructor(
    private formbilder: FormBuilder,
    private registerservice: RegisterentrepriseService,
    private activateroute: ActivatedRoute,
    private route: Router
  ) {}

  ngOnInit(): void {
    //update
    this.formupdate = this.formbilder.group({
      fullname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      photo: ['', Validators.required],
      phone: ['', Validators.required],
      git: ['', Validators.required],
      linkedIn: ['', Validators.required],
      adresse: ['', Validators.required],
    });
    this.formupdate.patchValue({
      fullname: this.userconnect.fullname,
      email: this.userconnect.email,
      password: this.userconnect.password,
      photo: this.userconnect.photo,
      phone: this.userconnect.phone,
      git: this.userconnect.git,
      linkedIn: this.userconnect.linkedIn,
      adresse: this.userconnect.adresse,
    });
  }
  handlefileinput(files: any) {
    this.filetoupload = <Array<File>>files.target.files;
    console.log(this.filetoupload);
  }

  updatecondidate() {
    //formupdate:FormGroup
    this.registerservice
      .updateprofil(this.userconnect._id, this.formupdate.value)
      .subscribe((res: any) => {
        console.log(res);
        localStorage.setItem('userconnect', JSON.stringify(res.data)); //bech ta3mel refrech l localstage
        Swal.fire('profile updated');
      });
  }
  deleteuser(id: any) {
    this.registerservice.deletecondidate(id).subscribe((res: any) => {
      localStorage.clear();
      console.log(res);
      Swal.fire('Deleted!');
      this.route.navigateByUrl('/login');
    });
  }
}
