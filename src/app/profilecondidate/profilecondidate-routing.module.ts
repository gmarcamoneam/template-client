import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfilecondidateComponent } from './profilecondidate.component';

const routes: Routes = [{ path: '', component: ProfilecondidateComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilecondidateRoutingModule { }
