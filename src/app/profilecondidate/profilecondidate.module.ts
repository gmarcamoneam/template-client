import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilecondidateRoutingModule } from './profilecondidate-routing.module';
import { ProfilecondidateComponent } from './profilecondidate.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProfilecondidateComponent
  ],
  imports: [
    CommonModule,
    ProfilecondidateRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ProfilecondidateModule { }
