import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilecondidateComponent } from './profilecondidate.component';

describe('ProfilecondidateComponent', () => {
  let component: ProfilecondidateComponent;
  let fixture: ComponentFixture<ProfilecondidateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilecondidateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProfilecondidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
