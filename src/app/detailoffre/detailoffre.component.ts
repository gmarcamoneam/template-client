import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OffreService } from '../offre.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-detailoffre',
  templateUrl: './detailoffre.component.html',
  styleUrls: ['./detailoffre.component.css'],
})
export class DetailoffreComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  id = this.activeroute.snapshot.params['id'];
  offre: any;
  listcommentaires: any;
  closeResult = '';
  modale: FormGroup;
  added: FormGroup;

  constructor(
    private activeroute: ActivatedRoute,
    private formbilder: FormBuilder,
    private offreservice: OffreService,

    private route: Router
  ) {}

  ngOnInit(): void {
    console.log('id', this.id);
    this.getoffresbyid();

    this.added = this.formbilder.group({
      description: ['', Validators.required],
      offre: ['', Validators.required],
      condidate: ['', Validators.required],
    });
  }
  getoffresbyid() {
    this.offreservice.getoffrebyid(this.id).subscribe((res: any) => {
      this.offre = res['data'];
      console.log('detail offre', this.offre);
    });
  }
  isconnect() {
    if (!(localStorage.getItem('state') == '0')) {
      this.route.navigateByUrl('/login');
    }
  }
  createcommentaire() {
    this.added.patchValue({
      offre: this.offre,
      condidate: this.userconnect._id,
    });
    this.offreservice
      .addedcommentaire(this.added.value)
      .subscribe((res: any) => {
        console.log(res['data']);

        Swal.fire('comment added !!');
      });
  }
}
