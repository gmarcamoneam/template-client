import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailoffreRoutingModule } from './detailoffre-routing.module';
import { DetailoffreComponent } from './detailoffre.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    DetailoffreComponent
  ],
  imports: [
    CommonModule,
    DetailoffreRoutingModule,
    FormsModule,
    ReactiveFormsModule
   // NgbModule
  ]
})
export class DetailoffreModule { }
