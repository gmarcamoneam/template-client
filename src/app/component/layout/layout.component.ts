import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FavoriteService } from 'src/app/favorite.service';
import { OffreService } from 'src/app/offre.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
})
export class LayoutComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  modale: FormGroup;
  filetoupload: Array<File> = [];
  listoffre: any;
  listcategory: any;
  listplace: any;
  listentreprise: any;
  listcondidate: any;
  listcontrat: any;
  recherche: any;
  p: any;
  term: any;
  items = [] as any;

  form: FormGroup;
  date = new Date().toISOString().split('T')[0].toString();
  constructor(
    private offreservice: OffreService,
    private favoritservice: FavoriteService,
    private formbuilder: FormBuilder,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.getalloffres();
    this.getallplace();
    this.getallentreprises();
    this.getallcategorys();
    this.getallcondidates();
    this.getallcontrats();

    this.form = this.formbuilder.group({
      category: ['', Validators.required],
      place: ['', Validators.required],
    });

    //added condidature
    this.modale = this.formbuilder.group({
      photo: ['', Validators.required],
      offre: ['', Validators.required],
      condidate: ['', Validators.required],
    });
  }
  getalloffres() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter((element: any) => {
        return (
          element.confirm == true &&
          Date.parse(element.dateexpiration) >= Date.parse(this.date)
        );
      });
      console.log('list of offre', this.listoffre);
    });
  }
  getallcategorys() {
    this.offreservice.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
      console.log('list of category', this.listcategory);
    });
  }
  getallplace() {
    this.offreservice.getallplace().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('list of place', this.listplace);
    });
  }
  getallentreprises() {
    this.offreservice.getallentrepris().subscribe((res: any) => {
      this.listentreprise = res['data'];
      console.log('list of entreprise', this.listentreprise);
    });
  }
  getallcondidates() {
    this.offreservice.getallcondidate().subscribe((res: any) => {
      this.listcondidate = res['data'];
      console.log('list of condidate', this.listcondidate);
    });
  }
  handlefileinput(files: any) {
    this.filetoupload = <Array<File>>files.target.files;
    console.log(this.filetoupload);
  }

  addfavorite(offre: any) {
    if (!this.favoritservice.offreinfavorit(offre))
      this.favoritservice.addtofavirit(offre);
    this.items = this.favoritservice.getitems();
    console.log(this.items);
    Swal.fire('offre edded to favorit');
  }
  createcondidature() {
    let formdata = new FormData();
    formdata.append('condidate', this.modale.value.condidate);
    formdata.append('offre', this.modale.value.offre);
    formdata.append('photo', this.filetoupload[0]);
    this.offreservice.addcondidature(formdata).subscribe((res: any) => {
      console.log(res['data']);
      Swal.fire('condidature created');
    });
  }
  isconect(p: any) {
    if (!(localStorage.getItem('state') == '0')) {
      this.route.navigateByUrl('/login');
    } else {
      this.open(p);
    }
  }
  open(p: any) {
    this.modale.patchValue({
      photo: p.photo,
      offre: p._id,
      condidate: this.userconnect._id,
    });
  }
  getallcontrats() {
    this.offreservice.getallcontrat().subscribe((res: any) => {
      this.listcontrat = res['data'];
      console.log('list of contrat', this.listcontrat);
    });
  }
  onchangebycontrat(e: any) {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter((el: any) => el.confirm == true);
      this.listoffre = this.listoffre.filter(
        (el: any) => el.contrat._id == e.target.value
      );
      console.log('list of contrat', this.listoffre);
    });
  }
  onchangebyplace(a: any) {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter((el: any) => el.confirm == true);
      this.listoffre = this.listoffre.filter(
        (el: any) => el.place._id == a.target.value
      );
      console.log('list of contrat', this.listoffre);
    });
  }
}
