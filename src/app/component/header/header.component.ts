import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(private route: Router) {}

  ngOnInit(): void {}
  logout() {
    localStorage.clear();
    this.route.navigateByUrl('/login');
  }
  isconnect() {
    return localStorage.getItem('state') == '0' ? true : false;
  }
  iscondidate() {
    return localStorage.getItem('role') == 'condidate' ? true : false;
  }
  isentreprise() {
    return localStorage.getItem('role') == 'entreprise' ? true : false;
  }
}
