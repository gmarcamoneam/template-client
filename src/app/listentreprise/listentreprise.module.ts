import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListentrepriseRoutingModule } from './listentreprise-routing.module';
import { ListentrepriseComponent } from './listentreprise.component';


@NgModule({
  declarations: [
    ListentrepriseComponent
  ],
  imports: [
    CommonModule,
    ListentrepriseRoutingModule
  ]
})
export class ListentrepriseModule { }
