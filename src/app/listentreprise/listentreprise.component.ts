import { Component, OnInit } from '@angular/core';
import { RegisterentrepriseService } from '../registerentreprise.service';

@Component({
  selector: 'app-listentreprise',
  templateUrl: './listentreprise.component.html',
  styleUrls: ['./listentreprise.component.css'],
})
export class ListentrepriseComponent implements OnInit {
  listentreprise: any;
  constructor(private registerentreprise: RegisterentrepriseService) {}

  ngOnInit(): void {
    this.getallentreprises();
  }
  getallentreprises() {
    this.registerentreprise.getallentreprise().subscribe((res: any) => {
      this.listentreprise = res['data']; //.filter((element: any) => ( element.entreprise))
      console.log('list of entreprise', this.listentreprise);
    });
  }
}
