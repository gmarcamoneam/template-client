import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListentrepriseComponent } from './listentreprise.component';

const routes: Routes = [{ path: '', component: ListentrepriseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListentrepriseRoutingModule { }
